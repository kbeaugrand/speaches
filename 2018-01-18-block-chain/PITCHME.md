# BlockChain
## Explained to my grand mother

---

### Quèsaco 

--- 

### Quèsaco 

* Technologie de stockage et de transmission d'information
* Transparente
* Securisée
* Décentralisée

---

### Quèsaco 
#### Transmission de l'information
#### Transparence

Contient l'historique de toutes les transaction entre les utilisateurs depuis sa création.

---

### Quèsaco 
#### Sécurisée

Cette base de données est chiffrée

---

### Quèsaco 
#### Décentralisée

Il n'y a aucun intermédiaire, chaque partie prenante détient la base de données et participe à la validation des transactions.


---

### Quèsaco 

La blockchain peut être assimilée à un grand livre comptable public, anonyme et infalsifiable.

--- 

### Comment ca marche ?

---

![The Blockchain](https://upload.wikimedia.org/wikipedia/commons/7/7a/Bitcoin_Block_Data.png)

---

![Blockchain Process](https://upload.wikimedia.org/wikipedia/commons/4/4e/Blockchain-Process.png)

---

### Comment ca marche ?
#### Le minage/mintage

* C'est l'opération de chiffrement des blocs
* Tous les partis prenant peuvent réaliser ce minage
* Le premier ayant donné le résultat est rétribué
* Le bloc est ensuite ajouté à la chaine

---

### Comment ca marche ?
#### Et le théorème de CAP dans tout cela ?

---

### Théorème de CAP

* Cohérence
* Disponibilité
* Tolérance au partitionnement

Note: 
* Cohérence tous les nœuds du système voient exactement les mêmes données au même moment ;
* Disponibilité : garantie que toutes les requêtes reçoivent une réponse;
* Tolérance au partitionnement : aucune panne moins importante qu’une coupure totale du réseau ne doit empêcher le système de répondre correctement.

--- 

### Comment ca marche ?

Les Blockchain sont donc : 
* Disponibles 
* Toléreantes à la distribution

--- 

### Comment ca marche ?

Mais les Blockchain ne sont jamais :
* Cohérentes.

Note: 
Au fil du temps, des utilisateurs différents peuvent voir des pseudo états courant différents. Par exemple l’incohérence arrive lorsque le hash d’un bloc nouvellement découvert n’a pas encore été relayé à tous les utilisateurs du système.

Ces instabilités temporaires sont un grand obstacle lorsqu’on implémente une cryptomonnaie, puisque à la fin il faudra bien finir par obtenir une consistance finale. Les incohérences sont physiologiques, et donc tolérées, mais doivent être transitoires, mieux encore éphémères.

---

### Comment ca marche ?
#### Le consensus

* Emission des blocs découvert
* Décourager l'utilisation des bloc intermédiaires
* Résoudre l'ambiguïté dans un temps relativement court

Note: 
* Un utilisateur qui a découvert un bloc est encouragé à l’émettre sur le réseau immédiatement sans le retenir
* On doit décourager l’utilisateur de découvrir les blocs des chaînes intermédiaires (il faut qu’il focalise sur les plus longues).
* Les règles de consensus doivent être construites de manière à résoudre l’ambiguïté qui se forme à la suite d’un fork de blockchain: lors d’un fork une des branches en compétition doit prendre le contrôle sur les autres dans un temps raisonnablement court.

--- 

### Comment ca marche ?
#### POW V.S POS

* Proof of Work
* Proof of Stack

---

#### Proof Of Work

* Trouver l'argument (entier) qui satisfiera une équation donnée
* On ne peut pas prédire la valeur de cet entier
* Le résultat doit être validé par plus de la moitié des membres

Note: 
C'est ce qui est utilisée lors de l'opération de minage.
La seule manière de trouver le bon argument et de tenter une sorte de Brute force avec plusieurs itérations
Le principe est que les frandeurs seront théoriquement moins nombreux que les mêmbres honnêtes

---

#### Proof Of Stack

* Trouver le bon timestamp qui satisfiera une equation donnée
* La validation de cette POS est faite à celui qui a la plus grande balance 

Note: 
Cela veut dire que le plus riche membre aurait en permanance un avantage.
Il existe cependant des méthodes de pondération

--- 

### Comment ca marche ?
#### POW V.S POS

Proof of Work: 
* Calculs très complexes
* Grande consommation d'énergie
* Sécurité garantie si personne ne possède plus de 51% des noeuds

---

### Comment ca marche ?
#### POW V.S POS

Proof of Stack: 
* Calculs très simples
* Consommation d'énergie très faibles
* Sécurité garantie si personne ne possède plus de 51% des ressources
* Pas de rétibution des validations des opérations

---

### Et les smart contracts ?

C'est : 
* un programme qui contrôle directement les actifs numériques

Note: 
* Un Contract définit les règles d'un accord entre plusieurs partis. 
* Un SmartContract fige ces règles dans la blockchain et exécute les transferts logsque les conditions contractuelles se vérifient

---

### Et les smart contracts ?
#### Automatisation et restrictions

* Les actions décrites sont automatiquement appliquées lorsque le contrat est exécuté
* Le contrat ne peut être exécuté que si toutes les conditions sont réunies

---

### Et les smart contracts ?
#### Restrictions et Oracle

* Conditions propres à la BlockChain (Solde, Date, ...)
* Conditions externes à la BlockChain (Evènement, prestation effectuée, ...)

Note: 
Cet oracle est déterminé : 
* Au préalable : un tiers de confiance connu des deux parties;
* Une base de données de confiance
* Vote parmis un ensemble de participants


---

### Et les smart contracts ?
#### Cas d'usages

--- 

#### Cas d'usages

* Transactions Immobilières
* Assurances
* Supply Chain
* ...