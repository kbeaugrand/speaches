# Buffer Overflow

---

## The top of the software security vulnerability of the past 25 years

---

## Buffer overflows

* 14% of all CVE | 
* 35% of CVSS Scrore 10 CVE | 

Note: 
SourceFire : 25 YEARS OF VULNERABILITIES: 1988-2012 
CVE : Common Vulnerabilities and Exposures
CVSS : Common Vulnerability Scoring System

--- 

### Top 10 vendors in terms of vulnerabilities

![Figure 1](2019-01-17-buffer-overflow/assets/figure-1.png)

*SourceFire : 25 YEARS OF VULNERABILITIES: 1988-2012

--- 

###  Vulnerabilities by Windows version

![Figure 2](2019-01-17-buffer-overflow/assets/figure-2.png)

*SourceFire : 25 YEARS OF VULNERABILITIES: 1988-2012

--- 

### Mobile phone vulnerability market share

![Figure 3](2019-01-17-buffer-overflow/assets/figure-3.png)

*SourceFire : 25 YEARS OF VULNERABILITIES: 1988-2012

--- 

###  Microsoft security bulletin publication date versus CVE publication date

![Figure 4](2019-01-17-buffer-overflow/assets/figure-4.png)

*SourceFire : 25 YEARS OF VULNERABILITIES: 1988-2012

Note:
Wannacry: Attaque lancée 2 mois après la publication de Microsoft.

--- 

###  Top 3 vulnerability types by year

![Figure 5](2019-01-17-buffer-overflow/assets/figure-5.png)

*SourceFire : 25 YEARS OF VULNERABILITIES: 1988-2012

Note:
XSS et SQLi dépassent le BO pendant 2 ans puis BO reviens.

---

# Key Concepts of Buffer Overflow

---

## Key Concepts of Buffer Overflow

* Occurs when there is more data in a buffer than it can handle, causing data to overflow into adjacent storage. |
* Can cause application/system crash or worse, give an entry point to an attacker. | 
* C/C++ are more susceptible to buffer overflow. | 

--- 

# Example

--- 

## C code

```c
char           A[8] = "";
unsigned short B    = 1979;
```

--- 

## What is in memory ?

![Sample 1](2019-01-17-buffer-overflow/assets/sample-1.png)

--- 

## Now store data to the buffer !

```c
strcpy(A, "excessive");
```

We are storing null terminated string of 9 characters in a 8 bytes buffer.

The 10th character is a null byte that have been added too.

--- 

## What is in memory ?

![Sample 1](2019-01-17-buffer-overflow/assets/sample-2.png)

--- 

## How to fix it ?

```c
strlcpy(A, "excessive", sizeof(A));
```

---

## How an attacker could exploit it ?

---

## How an attacker could exploit it ?

* Stack-based exploitation |
* Heap-based exploitation | 

---

# Demo
## Stack-based exploitation

---

## Stack-based exploitation
### Typical memory layout

![index](2019-01-17-buffer-overflow/assets/index.png)

---

## Stack-based exploitation
### Attackers input exceeds user buffer

![index](2019-01-17-buffer-overflow/assets/index2.png)

---

## Stack-based exploitation
### Attacker creates tailored input

![index](2019-01-17-buffer-overflow/assets/index5.png)

---

## Stack-based exploitation
### Attackers input overwrites EIP with their own address pointing to the start of their shellcode

![index](2019-01-17-buffer-overflow/assets/index6.png)

---

# How to mitigate ?

---

## How to mitigate ?

### Developper side

* Choose a high level programming language |
* Use safe libraries |
* Compile with Space Address Protection |

Note: 
- Unmanaged code

---

## How to mitigate ?

### System side

* Use an operating system that implements Space Address Protection | 

--- 

### Space Address Protection ?

- DEP : Data Execution Prevention |
- ASLR : Address Space Layout Randomization | 

Note: 
- DEP : à partir de Windows Server 2003
- ASLR : à partir de Windows Server 2008

---

### Exemple of Famous BO Vuln.

* Morris worm (1988) |
* SQL Slammer (2003) |
* Wannacry (2017) |

Note: 
- Morris : fingerd service sur Unix-> Reverse Shell and DoS attack, self-replicating
- SQL Slammer : MSSQL Server -> DoS Attack
- Wannacry : SMB - EternalBlue -> Reverse Shell/ Ransomeware, self-replicating

