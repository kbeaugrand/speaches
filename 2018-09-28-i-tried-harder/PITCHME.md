## Offensive Security

---

## Offensive Security

The offensive Security aims to

* demonstrate the ability to research the network (information gathering) |
* identify any vulnerabilities and successfully execute attacks |
* compromise the systems and gain administrative access |

---

### Methodology

---

### Methodology

Once target identified

* Information gathering |
* Information gathering |
* Information gathering |
* Find a vulnerability |
* Exploit the vulnerability |
* Gain shell access (unpriviledged) |

---

### Methodology

Once shell access obtained

* Information gathering (again yes, three times) |
* Privilege escalation |
* ***(Nice stuff)*** |
* Post Exploitation |
* Pivoting |
* Redoing all steps |

---

### Methodology explained
#### Information gathering

Passive

* Open Web (Google) |
* Email Haresting |

---

### Methodology explained
#### Information gathering

Active

* DNS Enumeration |
* SNMP Enumeration |
* Port scanning |
* SMB Enumeration |
* SMTP Enumeration |
* Vulnerability Scanning (nmap, OpenVAS) |

---

### Methodology explained
#### Vulnerability Exploitation

* Cross-Site Scripting |
* File Inclusion |
* SQL Injection |
* Buffer Overflow |
* Combination |
* Existing Features |
* Metasploit Framework |

---

### Methodology explained
### Privilege Escalation

* Vulnerable applications |
* Bad configuration |
* Suid executable files |
* Scheduled tasks |
* TCP dump |
* ... |

---

### Methodology explained
### Post exploitation

* Hash dump |
* Sensitive files |
* DB dump |
* Password Hash attacks |
* Client Side attacks |
* ... |

---

### Methodology explained
#### Pivoting

* Port tunneling |
* Local port Forwarding |
* Remote Port Forwarding |
* Dyanmic Port Forwarding |
* Proxychains |
* HTTP Tunelling |
* Traffic Encapsulation |

+++

#### Pivoting
##### Local port Forwarding

![local-port-forwarding](2018-09-28-i-tried-harder/assets/local-port-forwarding.jpg)

+++

#### Pivoting
##### Remote port Forwarding

![remote-port-forwarding](2018-09-28-i-tried-harder/assets/remote-port-forwarding.jpg)

+++

#### Pivoting
##### Dyanmic port Forwarding


![dynamic-port-forwarding](2018-09-28-i-tried-harder/assets/dynamic-port-forwarding.jpg)

+++

#### Pivoting
##### HTTP Tunneling

![http-tunneling](2018-09-28-i-tried-harder/assets/http-tunneling.jpg)

---

### Penetration test report

* must be comprehensive |
* containing in-depth note and screeshot |
* with proof on level obtained |

---

### The Certification

---

### The Certification
**OSCP** for Offensive Security Certified Professional

---

### OSCP

* Course: Penetration Testing with Kali Linux (PWK) |
* Lab Access (min 30 days enrollement) |
* Earn after passing the 24-hour performance based exam |
* Submit a penetration test report |

---

### OSCP

OSCP demonstrates

* a certain degree of persistence and determination |
* the ability to think “outside the box” and “laterally.” |

---

### Lab

---

![penetration-network](2018-09-28-i-tried-harder/assets/penetration-network.jpg)
